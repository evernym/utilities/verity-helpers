# Scripts
 * dynamodb.sh
 * ledger.sh
 * mysql_wallet.sh
 * provision_verity_helper.py - This is a wrapper that executes the `provision_verity` image with the proper vars set, and looks up sensitive SEED data from vault
   - Example: `./provision_verity_helper.py -e demo -i "Customer Name" gen_tokens`
# Docker images
## provision_verity
See [Provision Verity README](provision_verity/README.md)

Example:    
`docker run --rm -it -e TASK=GEN_TOKENS -e SEED=<SEE README> -e EVERNYM_ID=<SEE README> -e ENTERPRISE_ID=<SEE README> -e TOKEN_COUNT=<SEE README> registry.gitlab.com/evernym/utilities/agencydev-helpers/provision_verity:latest`
