# Provision Token
# Intro
Helper scripts for provisioning task for Verity Application

# Parameters
All parameters are pass via Environment Variable

# Tasks
The script can run currently two tasks:
1. `GEN_TOKENS` - creates tokens for provisioning for the Verity Application
1. `REST_PROVISION` - provisions an agent, creates and formats an API key for that agent

The task is specified by the `TASK` environment variable. Each task has different parameters and different output. See below.

## GEN_TOKENS

### Available Parameters
* `SEED`: Is the seed of a key that is used to generate the token. The public part of this key pair is configured on the Verity Application an is used to validate the token during provisioning. This seed is known by Evernym only and is used to sign the token. 
* `EVERNYM_ID`: Is the sponsor id for Evernym, must match the id used in the configuration of the Verity Application for Evernym Sponsor information.
* `ENTERPRISE_ID`: An ID for the entity that is provisioning (can be any string, is recorded in the provisioned agent)
* `TOKEN_COUNT`: Number of tokens created

### Output
**When successful**, the script prints on stdout a JSON list of tokens (an nothing else). Each entry in the list is a single token and the whole token (the whole JSON object) must be given to the customer to be used in the provisioning process.

Example: 
```json
[
    {
        "sponseeId": "acme",
        "sponsorId": "EV_TEST",
        "nonce": "Dqn2mTcLbU2gEqI66QUkZCSSTJS5aZVL",
        "timestamp": "2020-06-19T19:09:02.965297",
        "sig": "z5ojavz8hVJ9dj8x/VSRy1YvbHvdvimhYCBspoLy7OK4cu+t+4T89qDIYhoO7FSDJ5j58YCEYhkIcXUT9COdDA==",
        "sponsorVerKey": "9sVaV45JGbEpf1L89AmMhBBUAFZvSL8h3TV3UUbmQuv1"
    },
    {
        "sponseeId": "acme",
        "sponsorId": "EV_TEST",
        "nonce": "6pzPUftlqsV1ITTrWrn5J9Yegxk8tbBj",
        "timestamp": "2020-06-19T19:09:02.965297",
        "sig": "tG2unImp3nMBSkFcOquc++IrM1kg0/O1g68tIqcV9vlDZRshlww/NSCJHrIAgXzdPQbjoEikzChdMtzljG46Dg==",
        "sponsorVerKey": "9sVaV45JGbEpf1L89AmMhBBUAFZvSL8h3TV3UUbmQuv1"
    }
]
```

**On failure**, a python exception is printed on stderr

## REST_PROVISION

### Available Parameters
Since provisioning the agent requires a token, the same parameters are need as `GEN_TOKENS` except for `TOKEN_COUNT`

* `SEED`: Is the seed of a key that is used to generate the token. The public part of this key pair is configured on the Verity Application an is used to validate the token during provisioning. This seed is known by Evernym only and is used to sign the token. 
* `EVERNYM_ID`: Is the sponsor id for Evernym, must match the id used in the configuration of the Verity Application for Evernym Sponsor information.
* `ENTERPRISE_ID`: An ID for the entity that is provisioning (can be any string, is recorded in the provisioned agent)
* `VERITY_URL`: The Verity Application instance that the agent is provisioned on
* `AGENT_SEED`: The seed for the verity-sdk key that is used during provisioning (this key is used for the REST Api key). `AGENT_SEED` is optional, if not provided then a random seed one will be generated and used

### Output
**When successful**, the script prints a JSON object that contains most of the verity-sdk context object (excluding the wallet name and key) and this object also includes the `apiKey`.

Example: 
```json
{
    "verityUrl": "http://vas.pqa.evernym.com",
    "verityPublicDID": "D6tuzxJe4Vpyz2XwTwnf7T",
    "verityPublicVerKey": "7bZHdWn2KNyD36iRxQSLqikFKmjFYfAyBjYJqw76Tfqg",
    "domainDID": "MdxmPHuUGxNm6B3vSEgHNS",
    "verityAgentVerKey": "CFPK5fwmE71JTu2aThfr9tQGPAsVbiCzCnrC5XHAtfcw",
    "sdkVerKeyId": "5jYV2GsnmFo6sEV25GcPAR",
    "sdkVerKey": "3adqhDJ2B2e19pKM6m3omZxFDWmBHhPjAvRT1p9kiWXU",
    "version": "0.2",
    "apiKey": "3adqhDJ2B2e19pKM6m3omZxFDWmBHhPjAvRT1p9kiWXU:3adqhDJ2B2e19pKM6m3omZxFDWmBHhPjAvRT1p9kiWXU3adqhDJ2B2e19pKM6m3omZxFDWmBHhPjAvRT1p9kiWXU"
}
```
**On failure**, a python exception is printed on stderr

# Docker Examples:
* `GEN_TOKENS`
```shell script
docker run -it --rm \
-e TASK=GEN_TOKENS \
-e SEED=HrWd38sqcr6FjQK1LNNCJxxkMbS2oR3W \
-e EVERNYM_ID=EV_TEST \
-e ENTERPRISE_ID=acme \
-e TOKEN_COUNT=2
registry.gitlab.com/evernym/utilities/agency-helpers/provision_verity:latest
```
* `REST_PROVISION`
```shell script
docker run -it --rm \
-e TASK=REST_PROVISION \
-e VERITY_URL=http://vas.pqa.evernym.com \
-e AGENT_SEED=CFPK5fwmE71JTu2aThfr9tQGPAsVbiCz \
-e SEED=Hr9WX2E3zbCKJd5YhNhub9DoueqWSf5D \
-e EVERNYM_ID=evernym-qa-sponsor \
-e ENTERPRISE_ID=acme \
registry.gitlab.com/evernym/utilities/agency-helpers/provision_verity:latest
```
