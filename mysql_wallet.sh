#!/bin/bash

## Variables ##
DATABASE_NAME="${DATABASE_NAME:-wallet}"
ENDPOINT="${ENDPOINT:localhost}"
ENDPOINT_PORT="${ENDPOINT_PORT:-3306}"
NEW_USER="${NEW_USER:-evernym_wallet}"
NEW_USER_PASS="${NEW_USER_PASS}"
ROOT_PASSWORD="${ROOT_PASSWORD}"
exit_on_action_error=true
args=$#
args_left=$args
to_do=()
to_do_args=()
doid=0

wallet_table="
CREATE TABLE IF NOT EXISTS wallets (
        id BIGINT(20) NOT NULL AUTO_INCREMENT,
        name VARCHAR(1024) NOT NULL,
        metadata VARCHAR(10240) NOT NULL,
        PRIMARY KEY (id),
        UNIQUE KEY wallet_name (name)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
"
items_table="
CREATE TABLE IF NOT EXISTS items (
        id BIGINT(20) NOT NULL AUTO_INCREMENT,
        wallet_id BIGINT(20) NOT NULL,
        type VARCHAR(128) NOT NULL,
        name VARCHAR(1024) NOT NULL,
        value LONGBLOB NOT NULL,
        tags JSON NOT NULL,
        PRIMARY KEY (id),
        UNIQUE KEY ux_items_wallet_id_type_name (wallet_id, type, name),
    CONSTRAINT fk_items_wallet_id FOREIGN KEY (wallet_id)
        REFERENCES wallets (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
"

tables=( "$wallet_table" "$items_table" )

## Functions ##
#Script Core Functions#
function register_action {
    args=""
    to_do[$doid]="$1"
    shift
    for arg in "$@" ; do
        if [ -z "$arg" ] ; then
            args="$args ''"
        else
            args="$args ${arg// /%20}"
        fi
    done
    to_do_args[$doid]=$args
    let doid+=1
}
function check_req_vars_set {
    local vars=( "$@" )
    local chk_val
    local errors=0
    for var in ${vars[@]} ; do
        while true; do
            eval "chk_val=\$$var"
            if [ -z "$chk_val" ] ; then
                    echo "Required Variable NOT set: $var" >&2
                    echo -n "Enter value for '$var': "
                    if echo "$var" | grep -qi 'pass'; then
                        read -s $var
                    else
                        read $var
                    fi
                    echo
            else
                break
            fi
        done
    done
}
function add_req_var {
    local var="$1"
    if [ -z "$req_vars" ] ; then
        req_vars=( $var )
    else
        for i in ${req_vars[@]} ; do
            if [ "$i" == "$var" ] ; then
                return
            fi
        done
        req_vars[$((${#req_vars[@]} + 1))]="$var"
    fi
}

#Main functionality bits
function install_mysql {
    apt-get update
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y mysql-server
}

function start_mysql {
    if nc -vz $ENDPOINT $ENDPOINT_PORT > /dev/null 2>&1 ; then
        return
    fi
    echo "Waiting for mysql  service to start on: ${ENDPOINT}:${ENDPOINT_PORT} "
    local count=0
    while ! nc -vz $ENDPOINT $ENDPOINT_PORT > /dev/null 2>&1; do
        if [ $count -ge 30 ] ; then
            echo "ERROR: mysql service is still not listening after 30 seconds... giving up"
            exit 1
        fi
        echo 'Still Waiting'
        sleep 1
        let count+=1
    done
    echo
}

function create_database {
    local database="${1:-$DATABASE_NAME}"
    echo "Creating database: '$database'"
    mysql -u root -h ${ENDPOINT} -P ${ENDPOINT_PORT} <<EOF
CREATE DATABASE IF NOT EXISTS $database;
EOF
    if [ $? -ne 0 ] ; then
        echo "Failed creating Database: ${database}"
        return 1
    fi
}

function create_user {
    local user="${1:-$NEW_USER}"
    local pass="${2:-$NEW_USER_PASS}"
    echo "Creating user: '$user'"
    mysql -u root -h ${ENDPOINT} -P ${ENDPOINT_PORT} <<EOF
CREATE USER IF NOT EXISTS '${user}'@'$FROM_HOST' IDENTIFIED BY '${pass}';
GRANT ALL PRIVILEGES ON ${DATABASE_NAME}.* TO '$user'@'$FROM_HOST';
FLUSH PRIVILEGES;
EOF
    if [ $? -ne 0 ] ; then
        echo "Failed, creating user. Aborting!"
        return 1
    fi
}

function create_schema {
    echo "Creating tables"
    for t in "${tables[@]}"; do
        echo -e "USE $DATABASE_NAME;\n$t" | mysql -u root -h ${ENDPOINT} -P ${ENDPOINT_PORT}
        mysql_rc=$?
        if [ $mysql_rc -ne 0 ] ; then
            echo "Failed creating table with definition"
            echo "$t"
            echo "Aborting!"
            return 1
            break
        fi
        sleep 0.5
    done
}

function provision_mysql {
    register_action install_mysql
    register_action start_mysql
    register_action create_database
    register_action create_user
    register_action create_schema
    add_req_var NEW_USER
    add_req_var NEW_USER_PASS
    add_req_var DATABASE_NAME
}

function help {
    cat <<EOF
Usage: $(basename $0) <action> <options>

ACTIONS:
    --create-user       Create a new user with access to the wallet database
        via $ENDPOINT
    --create-database   Create a new database with the wallet schema in it
    --create-schema     Create tables/schema in the database $DATABASE_NAME
    --endpoint <host>
        Connect to <host> for setting up tables etc...Default=localhost
    --endpoint-port <port>
        Connect to <port> on the host for setting up tables etc.. Default=3306
    --from-host
        When running with --create-user, specifically set the host that be
        using the new user to something other than $ENDPOINT
    --provision         Run through the initial provisioning steps to install
        mysql, create the database, create the wallet schema tables, and a
        database user with access it
    --use-root-password,--require-root-password
        When connecting to endpoint make sure that root password is set.
    --help              Display this help and exit

OPTIONS:
    --env-file          Source this environment file to get things like
        variables from it.

ENVIRONMENT VARIABLES:
    DATABASE_NAME       Specify the name of the database used when creating
        creating the database, or creating users who will access it.
        Default = $DATABASE_NAME
    NEW_USER            Set the username to use when creating users to access
        the database $DATABASE_NAME. Default = $NEW_USER
    NEW_USER_PASS       Set the password to use when create the user.
    ROOT_PASSWORD       Set the password to use when connecting as the 'root' user
EOF
}

## - Main - ##
while [ $args_left -ge 1 ] ; do
    case "$1" in
        --env-file)
            if [ -f "$2" ] ; then
                source "$2"
                shift
                let args_left=args_left-1
            else
                echo "ERROR: Cannot find environment file: '$2'"
                exit 1
            fi
            ;;
        --create-user)
            register_action start_mysql
            register_action create_user
            add_req_var NEW_USER
            add_req_var NEW_USER_PASS
            ;;
        --create-database)
            register_action start_mysql
            register_action create_database
            add_req_var DATABASE_NAME
            ;;
        --create-schema)
            register_action start_mysql
            register_action create_schema
            add_req_var DATABASE_NAME
            ;;
        --endpoint)
            if [ ! -z "$2" ] ; then
                ENDPOINT="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        --endpoint-port)
            if [ ! -z "$2" ] ; then
                ENDPOINT_PORT="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        --provision)
            provision_mysql
            ;;
        --from-host)
            if [ ! -z "$2" ] ; then
                FROM_HOST="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        --use-root-password|--require-root-password)
            add_req_var ROOT_PASSWORD
            ;;
        --help|-h)
            help
            exit 0
            ;;
        *)
            echo "Unknown argument: $1"
    esac
    shift
    let args_left=args_left-1
done

if [ ${#to_do[@]} -eq 0 ] ; then
    echo "No action was specified:"
    help
    exit 1
fi

#Check for any missing required vars
if ! check_req_vars_set ${req_vars[@]} ; then
    echo -e "Missing some required variables!\nSet corresponding options and try again.\nexiting...\n\n"
    exit 2
fi

if [ ! -z "$ROOT_PASSWORD" ] ; then
    export MYSQL_PWD="$ROOT_PASSWORD"
fi

# Run our to_do list
for ((id=0; id<$doid; id++)) ; do
    action=${to_do[$id]}
    opts=( ${to_do_args[$id]} )
    for ((oid=0; oid<${#opts[*]}; oid++)) ; do
        op=${opts[$oid]}
        if [ "$op" = "''" ] ; then
            opts[$oid]=""
        else
            opts[$oid]=${op//%20/ }
        fi
    done
        if [ "$disp_play_by_play" == "true" ] ; then
            echo "Running action: $action \"${opts[@]}\""
        fi
    $action "${opts[@]}"
    rc=$?
    if [ "$exit_on_action_error" == "true" ] ; then
        if [ $rc -gt 0 ] ; then
            echo "ERROR: Received return code of $rc running action: $action and opts: ${opts[@]}"
            exit 1
        fi
    fi
done
