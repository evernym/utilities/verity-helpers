#!/usr/bin/env python3
import asyncio
import json
import os
import faulthandler
import time
from provision_verity.provision.gen_token import gen_multiple_tokens, task_args as gen_tokens_args
from provision_verity.provision.provision_rest import provision_rest, task_args as rest_args


def get_args(expected_args, args=os.environ):
    rtn = dict()
    for key, val in expected_args.items():
        arg = args.get(key, val[0])
        if arg is None:
            raise Exception(val[1])
        else:
            rtn[key] = arg.strip()
    return rtn


async def main(env=os.environ):
    task = env.get("TASK", None)
    if task == "GEN_TOKENS":
        args = get_args(gen_tokens_args, env)
        return await gen_multiple_tokens(args)
    elif task == "REST_PROVISION":
        args = get_args(rest_args, env)
        return await provision_rest(args)
    else:
        raise Exception("Unknown task -- " + str(None))


if __name__ == '__main__':
    faulthandler.enable()
    mainloop = asyncio.get_event_loop()
    out = mainloop.run_until_complete(main())
    multiline = os.environ.get('MULTILINE_OUTPUT', 'false')
    if isinstance(out, list):
        if os.environ.get('DOTENV_OUTPUT') is not None:
            for elem, token in enumerate(out, 1):
                print("TOKEN_{}=".format(str(elem).zfill(2))+json.dumps(token))
        else:
            for elem, token in enumerate(out, 1):
                print("Token #{}".format(elem))
                if multiline == 'true':
                    print(json.dumps(token, indent=4))
                else:
                    print(json.dumps(token))
                print('')
    else:
        if multiline == 'true':
            print(json.dumps(out, indent=4))
        else:
            print(json.dumps(out))
    time.sleep(1)   # waiting for libindy thread to complete