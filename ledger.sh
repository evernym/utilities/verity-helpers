#!/bin/bash

## Variables ##
exit_on_action_error=true
disp_play_by_play=false
args=$#
args_left=$args
to_do=()
to_do_args=()
doid=0
provision=false
INTERNAL_IP=$(ip addr | grep 'inet ' | tail -n 1 | awk '{print $2}' | cut -d '/' -f 1)
INDY_STARTING_PORT=9701
LEDGER_NAME="${LEDGER_NAME:-agency-devlab}"
SOVTOKEN_MANAGE_DIR="$(dirname $0)"
FEES_ENABLE=true
MINT_TOKENS=true
GENESIS_FILE="${GENESIS_FILE}"
WALLET_KEY="${WALLET_KEY:-d3vl4b}"

## Functions ##
#Script Core Functions#
function add_req_var {
    local var="$1"
    if [ -z "$req_vars" ] ; then
        req_vars=( $var )
    else
        for i in ${req_vars[@]} ; do
            if [ "$i" == "$var" ] ; then
                return
            fi
        done
        req_vars[$((${#req_vars[@]} + 1))]="$var"
    fi
}
function check_req_vars_set {
    local vars=( "$@" )
    local chk_val
    local errors=0
    for var in ${vars[@]} ; do
        while true; do
            eval "chk_val=\$$var"
            if [ -z "$chk_val" ] ; then
                    echo "Required Variable NOT set: $var" >&2
                    echo -n "Enter value for '$var': "
                    if echo "$var" | grep -qi 'pass'; then
                        read -s $var
                    else
                        read $var
                    fi
            else
                break
            fi
        done
    done
}
function register_action {
    args=""
    to_do[$doid]="$1"
    shift
    for arg in "$@" ; do
        if [ -z "$arg" ] ; then
            args="$args ''"
        else
            args="$args ${arg// /%20}"
        fi
    done
    to_do_args[$doid]=$args
    let doid+=1
}

## Helper Functions ##

function add_sovrin_repo {
    local update=false
    if [ -z "$(apt-key list 68DB5E88 2>/dev/null)" ] ; then
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 68DB5E88
    fi
    if [ ! -f /tmp/repo_added ] ; then
        if ! grep -q 'deb https://repo.sovrin.org/deb xenial stable' /etc/apt/sources.list.d/sovrin.list 2>/dev/null; then
            echo "deb https://repo.sovrin.org/deb xenial stable" >> /etc/apt/sources.list.d/sovrin.list
            update=true
        fi
        if [ $? -ne 0 ] ; then
            return 1
        fi
        if [ "$update" == true ] ; then
            apt-get update
        fi
        touch /tmp/repo_added
    fi
    apt-get update
}

function add_sovrin_sdk_repo {
    local update=false
    if [ -z "$(apt-key list 68DB5E88 2>/dev/null)" ] ; then
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 68DB5E88
    fi
    if [ ! -f /tmp/sdk_repo_added ] ; then
        if ! grep -q 'deb https://repo.sovrin.org/sdk/deb xenial stable' /etc/apt/sources.list.d/sovrin.list 2>/dev/null; then
            echo "deb https://repo.sovrin.org/sdk/deb xenial stable" >> /etc/apt/sources.list.d/sovrin.list
            update=true
        fi
        if [ $? -ne 0 ] ; then
            return 1
        fi
        if [ "$update" == true ] ; then
            apt-get update
        fi
        touch /tmp/sdk_repo_added
    fi
}

function enable_indy_node {
    for i in {1..4} ; do
        if systemctl is-active indy-node${i}; then
            systemctl enable indy-node${i}.service
        else
            echo "ERROR: service 'indy-node${i}' failed to start. Aborting!"
            return 1
        fi
    done
}

function find_genesis_file {
    if [ -z "$GENESIS_FILE" ] ; then
        echo "Looking for genesis file" >&2
        if [ -f '/var/lib/indy/genesis.txt' ] ; then
            pool_genesis='/var/lib/indy/genesis.txt'
        elif [ -f '/devlab/shared_configs/ledger/genesis.txn' ] ; then
            pool_genesis='/devlab/shared_configs/ledger/genesis.txn'
        else
            pool_genesis=$(find /var/lib/indy -maxdepth 2 -type f -name 'pool_transactions_genesis' | grep -vE 'local|live|sandbox')
        fi
        if [ -z "$pool_genesis" ] ; then
            echo "Unable to find genesis file, aborting!" >&2
            return 1
        fi
    else
        pool_genesis="$GENESIS_FILE"
    fi
    export GENESIS_FILE="$pool_genesis"
}

function get_cli_mode {
    local cli_modes=( '1_4' '1_5' )
    local cli_mode=''
    local cli_mode
    if [ -z "${INDY_CLI}" ] ; then
        echo "No indy-node version set, defaulting mode to latest: ${cli_modes[-1]}"
        cli_mode="${cli_modes[-1]}"
    fi
    if [ -z "$cli_mode" ] ; then
        cli_mode="$(get_mode_str "${INDY_CLI}" "${cli_modes[*]}")"
    fi
    echo "$cli_mode"
}

function get_mode_str {
    local version="$1"
    local modes=( $2 )
    local m_split
    local m
    local ver_split=( ${version//./ } )
    local maj
    local min
    local v_maj=${ver_split[0]}
    local v_min=${ver_split[1]}
    if [ -z "$version" ] ; then
        return
    fi
    for m in "${modes[@]}"; do
        m_split=( ${m//_/ } )
        maj=${m_split[0]}
        min=${m_split[1]}
        if [ $v_maj -le $maj ] ; then
            if [ $v_min -le $min ] ; then
                echo "${maj}_${min}"
                return 0
            fi
        fi
    done
    echo ${modes[-1]}
    return 1
}

function get_node_mode {
    local node_modes=( '1_3' '1_5' )
    local node_mode
    local node_mode=''
    if [ -z "${INDY_NODE}" ] ; then
        echo "No indy-node version set, defaulting mode to latest: ${node_modes[-1]}"
        node_mode="${node_modes[-1]}"
    fi
    if [ -z "$node_mode" ] ; then
        node_mode="$(get_mode_str "${INDY_NODE}" "${node_modes[*]}")"
    fi
    echo "$node_mode"
}

function gen_pkgs_to_install {
    local package=$1
    local pkgs=''
    local ps=( $(apt-cache show $package | grep Depends | head -n 1 | sed 's/^Depends:// ; s/([<>]=[^()]\+)/ /g; s/([<>]\{1,2\}[^()]\+)//g ; s/[() ]//g; s/,/ /g') )
    local pstr="$package"
    local p
    local dps
    for p in "${ps[@]}"; do
        if [[ $p =~ '=' ]]; then
            pstr="$pstr\n$p"
            dps=$(gen_pkgs_to_install $p)
            echo "package: $package needs: ${dps//$'\n'/,}" >&2
            pstr="$pstr\n$dps"
        fi
    done
    echo -e "$pstr" | sort -u
}

function help {
    cat <<EOF
Usage: $(basename $0) <action> <options>

ACTIONS:
    --help,-h           Display this help and exit
    --install-indy,--install,-i
                        Install the indy-node,
    --install-indy-cli,--cli,-I
                        Install indy-cli
    --provision,-p      Run through the provisioning steps that will install
                        needed packages, and configure the ledger
    --seed-dids,--seed-ledger
                        Make sure that indy-cli is installed,then add embedded
                        seeds to the wallet, and ensure they are on the ledger
    --set-fees,-f       Run the sovtoken_manage.sh script with set-fees opts
    --set-taa,-t <CONTENT>
                        Set a TAA on the ledger. If CONTENT is a path to a file
                        then the contents of that file will be loaded. Else the
                        text will be the content of the TAA
    --start-indy-node,--start-indy,--start
                        Start all of the indy-node services
    --stop-indy-node,--stop-indy,--stop
                        Stop all of the indy-node services
    --unset-taa,-T      Unset a TAA from the ledger.

OPTIONS:
    --env-file          Source this environment file to get things like
                        variables from it.
    --genesis-file      File to use when creating a pool in indy-cli
    --indy-node-version,--indy-node
                        Set the version of indy-node that should be installed
                        for actions that install indy packages.
    --indy-cli-version,--indy-cli
                        Set the version of indy-cli that should be installed
                        for actions that install indy
    --libindy-version,--libindy
                        Set the version of libindy that should be installed for
                        actions that install indy. Needed by indy-cli
    --libsovtoken-version,--libsovtoken
                        Set the version of libsovtoken that should be installed
                        for actions that set up fees
    --skip-fees,-F      Skip setting fees when running --provision
    --sovtoken-version,--sovtoken
                        Set the version of sovtoken that should be installed
                        for actions that set up fees
    --sovtokenfees-version,--sovtokenfees
                        Set the version of sovtoken that should be installed
                        for actions that set up fees

ENVIRONMENT VARIABLES:

    INTERNAL_IP         Override the default lookup of the internal IP to use
                        for setting up indy-node. Default=$INTERNAL_IP
    INDY_STARTING_PORT  Override the default starting port for indy-node to
                        listen on. Default=$INDY_STARTING_PORT
    EXTERNAL_IP         When setting up indy-node, use this IP as the external
                        ip address, instead of the INTERNAL_IP.
    GENESIS_FILE        Path to genesis transaction file to use for indy-cli
                        pool creation etc...
    LEDGER_NAME         When setting up indy-node, use this as the ledger/pool
                        name. Default=$LEDGER_NAME
    WALLET_KEY          When setting fees, or preseeding the indy user's wallet
                        use this key for creating, and opening the wallet.

    #Used for external script sovtoken_manage.sh invocations
    SOVTOKEN_MANAGE_DIR Override the default path of the sovtoken_manage.sh
                        script used set fees on the ledger.
                        Default=$SOVTOKEN_MANAGE_DIR
    FEES_ENABLE         Can be 'true' or 'false', indicating whether or not
                        fees should be set when running with --set-fees.
    FEES                The fees to be set on the ledger during --set-fees
                        actions
    MINT_TOKENS         Can be 'true' or 'false', indicating whether or not
                        tokens should be minted
    MINT_TOKENS_AMOUNT  Amount of tokens to set when using --mint
    PAYMENT_ADDRESS     Destination for new token being minted

    #Defines which version of packages to install
    INDY_NODE
    INDY_CLI
    LIBINDY
    LIBSOVTOKEN
    SOVTOKEN
    SOVTOKENFEES

EOF
}

function install_indy {
    #This grabs all of the dependent packages and their explicit versions
    local node_version="${INDY_NODE}"
    local packages
    local dpkg_out
    if [ ! -z "$node_version" ] ; then
        node_version="=$node_version"
    fi
    echo "Generating list of explicitly defined versions"
    dpkg_out=$(/usr/bin/dpkg-query -f '${Package}=${Version}\n' -W indy-node 2>/dev/null)
    if ! echo "$dpkg_out" | grep -q "indy-node${node_version}" ; then
        packages=$(gen_pkgs_to_install indy-node${node_version})

        apt-get install --no-install-recommends -y $packages
    fi
}

function install_indy_client {
    local install_pkgs=false
    local indycli_version
    local libindy_version
    local cli_dpkg_out
    local li_dpkg_out
    indycli_version="${INDY_CLI}"
    if [ ! -z "$indycli_version" ] ; then
        indycli_version="=$indycli_version"
    fi
    libindy_version="${LIBINDY}"
    if [ ! -z "$libindy_version" ] ; then
        libindy_version="=$libindy_version"
    fi
    if [ ! -f /tmp/indy_client ] ; then
        cli_dpkg_out=$(/usr/bin/dpkg-query -f '${Package}=${Version}\n' -W indy-cli 2>/dev/null)
        li_dpkg_out=$(/usr/bin/dpkg-query -f '${Package}=${Version}\n' -W libindy 2>/dev/null)
        add_sovrin_sdk_repo
        if ! echo "$cli_dpkg_out" | grep -q "indy-cli${indycli_version}" ; then
            cli_txt="indy-cli${indycli_version}"
            install_pkgs=true
        fi
        if ! echo "$li_dpkg_out" | grep -q "libindy${libindy_version}" ; then
            li_txt="libindy${libindy_version}"
            install_pkgs=true
        fi
        if [ "$install_pkgs" == true ] ; then
            apt-get install --no-install-recommends -y $cli_txt $li_txt
            if [ $? -ne 0 ] ; then
                return 1
            fi
        fi
        touch /tmp/indy_client
    fi
}

function make_indy_user {
    adduser --system --group --home /home/indy --shell /bin/bash indy
}

function mint_tokens {
    local libsovtoken_version="${LIBSOVTOKEN}"
    if [ ! -z "$libsovtoken_version" ] ; then
        libsovtoken_version="=$libsovtoken_version"
    fi
    libsovtoken_dpkg=$(/usr/bin/dpkg-query -f '${Package}=${Version}\n' -W libsovtoken 2>/dev/null)
    if ! echo "$libsovtoken_dpkg" | grep -q "libsovtoken${libsovtoken_version}" ; then
        apt-get install --no-install-recommends -y libsovtoken${libsovtoken_version}
    fi
    if id indy > /dev/null 2>&1 ; then
        ${SOVTOKEN_MANAGE_DIR}/sovtoken_manage.sh --setup-sovtoken-only --sovtoken-vers "$SOVTOKEN" --sovtokenfees-vers "$SOVTOKENFEES"
        su - indy -c "${SOVTOKEN_MANAGE_DIR}/sovtoken_manage.sh --seed-trustees --pool-name ${LEDGER_NAME} --wallet-key '${WALLET_KEY}' --genesis-path '$GENESIS_FILE' --mint --tokens '${MINT_TOKENS_AMOUNT}' --payment-address '${PAYMENT_ADDRESS}'"
    else
        if ! find_genesis_file ; then
            return 1
        fi
        ${SOVTOKEN_MANAGE_DIR}/sovtoken_manage.sh --seed-trustees --pool-name ${LEDGER_NAME} --wallet-key "${WALLET_KEY}" --genesis-path "$GENESIS_FILE" --mint --tokens "${MINT_TOKENS_AMOUNT}" --payment-address "${PAYMENT_ADDRESS}"
    fi
}

function parse_text_table {
    local hl=0
    local prev_cmd
    while read line; do
        if [[ "$line" =~ ^\+-+ ]] ; then
            let hl+=1
            continue
        else
            if [[ $hl == 0 ]] ; then
                prev_cmd="$line"
            fi
        fi
        if [ -z "$line" ] ; then
            cmd_pref=''
            hl=0
            continue
        fi
        if [[ $hl -ge 2 ]] ; then
            echo "$line" | sed 's/^| //; s/ |$//; s/ //g' | sed "s/^/${prev_cmd// /%20}|/"
        fi
    done
}

function preseed_indy_wallet {
    install_indy_client
    if [ $? -ne 0 ] ; then
        echo "Errors occurred trying to install indy-cli, aborting"
        return 1
    fi
    setup_wallet
    ##Preseeding our DID's for use by our various components.
    echo "Preseeding our DID's for use by our various components."
    printf -v cli_context "wallet open default key=${WALLET_KEY}\npool connect ${LEDGER_NAME}\n"
    cur_dids=$(run_indy_cmd "${cli_context}did list" true)
    cur_did_array=()
    dids_to_add=(
        "TGLBMTcW9fHdkSqown9jD8,~LUFonrpk5JYtKGTVTn7bhu" # CAS
        "CV65RFpeCtPu82hNF9i61G,~NkTdu6XpFYmcBHV3NfLL34" # EAS
        "Rgj7LVEonrMzcRC1rhkx76,~XrwsWm73DykapmVTvurL6q,TRUST_ANCHOR" # VAS
        "X7a54Z11JuYXJ243M2HpJD,~DSM1nxmkAEwMmNbLRbWKwf,TRUST_ANCHOR" # VUI
    )
    did_exists=false
    if [ ! -z "$cur_dids" ] ; then
        cur_did_array=( $cur_dids )
    fi
    for de in "${cur_did_array[@]}" ; do
        d_split=( ${de//|/ } )
        did="${d_split[1]}"
        if [ "$did" == "V4SGRU86Z58d6TV7PBUe6f" ] ; then
            did_exists=true
            break
        fi
    done
    if [ "$did_exists" == false ] ; then
        new_did_out=$(run_indy_cmd "${cli_context}did new seed=000000000000000000000000Trustee1")
        if [ $? -ne 0 ] ; then
            echo "$new_did_out"
            echo "Error, adding seed to wallet :( Aborting..." >&2
            exit 1
        fi
    fi
    printf -v cli_context_did_ledger "${cli_context}did use V4SGRU86Z58d6TV7PBUe6f\n"
    for did_info in "${dids_to_add[@]}"; do
        did_info=( ${did_info//,/ } )
        did="${did_info[0]}"
        verkey="${did_info[1]}"
        role="${did_info[2]}"
        if [ ! -z "$role" ] ; then
            role="role=${role}"
        fi
        echo "Checking to see if DID: ${did} is already on the ledger"
        ledger_out=$(run_indy_cmd "${cli_context_did_ledger}ledger get-nym did=${did}")
        if [ $? -eq 0 ] ; then
            echo "DID: ${did} already exists on the ledger. Skipping"
            continue
        fi
        echo "Adding entry to ledger: did=${did} verkey=${verkey} ${role}"
        ledger_out=$(run_indy_cmd "${cli_context_did_ledger}ledger nym did=${did} verkey=${verkey} ${role}")
        if [ $? -ne 0 ] ; then
            echo "$ledger_out"
            echo "Error while trying to add DID: '${did}' to the ledger. Aborting!!" >&2
            exit 1
        fi
    done
}

function provision_ledger {
    register_action add_sovrin_repo
    register_action make_indy_user
    register_action install_indy
    register_action setup_indy_node
    register_action preseed_indy_wallet
    add_req_var INTERNAL_IP
    add_req_var INDY_STARTING_PORT
    add_req_var LEDGER_NAME
    add_req_var WALLET_KEY
}

function run_indy_cmd {
    local cmd="$1"
    local parse_table=$2
    local hl
    local out
    local pout
    local cmd_pref
    local prev_line

    #Read from stdin if $cmd is empty
    if [ -z "$cmd" ] ; then
        while read line; do
            printf -v cmd "${cmd}\n${line}"
        done
    fi
    cli_res=$(indy-cli --config <(echo '{"taaAcceptanceMechanism": "at_submission"}') <<EOF
$cmd
EOF
)
    # Check to see if we got an error
    if echo "$cli_res" | grep -qi 'execution failed' ; then
        echo "$cli_res"
        return 1
    fi
    if [ "$parse_table" == true ] ; then
        out=$(echo "$cli_res" | parse_text_table)
    else
        out="$cli_res"
    fi
    if [ ! -z "$out" ] ; then
        echo "$out"
    fi
}

function set_fees {
    local libsovtoken_version="${LIBSOVTOKEN}"
    if [ ! -z "$libsovtoken_version" ] ; then
        libsovtoken_version="=$libsovtoken_version"
    fi
    libsovtoken_dpkg=$(/usr/bin/dpkg-query -f '${Package}=${Version}\n' -W libsovtoken 2>/dev/null)
    if ! echo "$libsovtoken_dpkg" | grep -q "libsovtoken${libsovtoken_version}" ; then
        apt-get install --no-install-recommends -y libsovtoken${libsovtoken_version}
    fi
    if id indy > /dev/null 2>&1 ; then
        ${SOVTOKEN_MANAGE_DIR}/sovtoken_manage.sh --setup-sovtoken-only --sovtoken-vers "$SOVTOKEN" --sovtokenfees-vers "$SOVTOKENFEES"
        su - indy -c "${SOVTOKEN_MANAGE_DIR}/sovtoken_manage.sh --seed-trustees --pool-name ${LEDGER_NAME} --wallet-key '${WALLET_KEY}' --fee '$FEES' --genesis-path '$GENESIS_FILE' --set-fees"
    else
        if ! find_genesis_file ; then
            return 1
        fi
        ${SOVTOKEN_MANAGE_DIR}/sovtoken_manage.sh --seed-trustees --pool-name ${LEDGER_NAME} --wallet-key "${WALLET_KEY}" --fee "$FEES" --genesis-path "$GENESIS_FILE" --set-fees
    fi
}

function set_taa {
    taa_content="$1"
    set_aml=false
    if [ -f "$taa_content" ] ; then
        taa_content=$(cat "$taa_content")
    fi
    install_indy_client
    setup_wallet
    printf -v cli_context "wallet open default key=${WALLET_KEY}\npool connect ${LEDGER_NAME}\n"
    cur_dids=$(run_indy_cmd "${cli_context}did list" true)
    if ! echo "$cur_dids" | grep -q 'V4SGRU86Z58d6TV7PBUe6f'; then
        echo "ERROR: DID: 'V4SGRU86Z58d6TV7PBUe6f' Not found in wallet. Maybe do --seed-dids first?" >&2
        exit 1
    fi
    ledger_out=$(run_indy_cmd "${cli_context}ledger custom {\"reqId\":1,\"identifier\":\"V4SGRU86Z58d6TV7PBUe6f\",\"operation\":{\"type\":\"6\"},\"protocolVersion\":2}")
    cur_taa_version=$(echo "$ledger_out" | grep -o '"data":{.*"version":"[0-9]\+\.[0-9]\+\.[0-9]\+"' | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+')
    if [ -z "$cur_taa_version" ] ; then
        echo "No TAA version found, defaulting to 1.0.0"
        taa_version="1.0.0"
        set_aml=true
    else
        echo "Found existing TAA with version: ${cur_taa_version}. Incrementing"
        taa_sub=${cur_taa_version##*\.}
        taa_sub=$((taa_sub + 1))
        taa_version="${cur_taa_version%\.*}.${taa_sub}"
    fi
    echo "New TAA version will be: $taa_version"
    printf -v cli_context_did_ledger "${cli_context}did use V4SGRU86Z58d6TV7PBUe6f\n"
    if [ $set_aml == true ] ; then
        echo "Setting TAA AML"
        aml='{"product_eula":"The agreement was included in the software products terms and conditions as part of a license to the end user.","service_agreement":"The agreement was included in the terms and conditions the user accepted as part of contracting a service.","at_submission":"The agreement was reviewed by the user and accepted at the time of submission of this transaction.","for_session":"The agreement was reviewed by the user and accepted at some point in the users session prior to submission.","wallet_agreement":"The agreement was reviewed by the user and this affirmation was persisted in the users wallet for use during submission.","on_file":"An authorized person accepted the agreement, and such acceptance is on file with the users organization."}'
        ledger_out=$(run_indy_cmd "${cli_context_did_ledger}ledger txn-acceptance-mechanisms aml=$aml version=3")
        if [ $? -ne 0 ] ; then
            echo "$ledger_out"
            echo "Error setting AML...Aborting" >&2
            exit 1
        fi
    fi
    echo -e "Setting TAA content version: ${taa_version} to:\n===\n'${taa_content}'\n==="
    ledger_out=$(run_indy_cmd "${cli_context_did_ledger}ledger txn-author-agreement version="${taa_version}" text=\"${taa_content}\"")
    if [ $? -ne 0 ] ; then
        echo "$ledger_out"
        echo "Error setting TAA...Aborting" >&2
        exit 1
    fi
}

function setup_indy_node {
    local node_mode=$(get_node_mode)
    local ext_ip=${1:-$EXTERNAL_IP}
    local port=${2:-$INDY_STARTING_PORT}
    local ledger_name=${3:-$LEDGER_NAME}
    local node_port
    local client_port
    local port
    #Default external IP to internal IP if not passed or empty.
    ext_ip=${ext_ip:-$INTERNAL_IP}
    if [ ! -d "/var/lib/indy/${LEDGER_NAME}" ] ; then
        if ! grep -q 'NETWORK_NAME' /etc/indy/indy_config.py; then
            echo "NETWORK_NAME='${LEDGER_NAME}'" >> /etc/indy/indy_config.py
        else
            sed -i "s/^NETWORK_NAME.*/NETWORK_NAME='${LEDGER_NAME}'/" /etc/indy/indy_config.py
        fi
        echo "Creating New Ledger"
        su - indy -s /bin/bash -c "
        init_indy_node Node1 9701 9702 000000000000000000000000000Node1
        init_indy_node Node2 9703 9704 000000000000000000000000000Node2
        init_indy_node Node3 9705 9706 000000000000000000000000000Node3
        init_indy_node Node4 9707 9708 000000000000000000000000000Node4
        generate_indy_pool_transactions --network ${LEDGER_NAME} --nodes 4 --clients 4 --nodeNum 1 2 3 4 --ips '${ext_ip},${ext_ip},${ext_ip},${ext_ip}'
        "
        echo -e '\nCreating systemctl start scripts...'
        for i in {1..4} ; do
            node_port=$port
            let port+=1
            client_port=$port
            let port+=1
            cat <<EOF > /etc/indy/indy${i}.env
NODE_NAME=Node${i}
NODE_IP=${INTERNAL_IP}
NODE_PORT=$node_port
NODE_CLIENT_IP=${INTERNAL_IP}
NODE_CLIENT_PORT=$client_port
CLIENT_CONNECTIONS_LIMIT=15360
EOF
            if [ "$node_mode" == "1_3" ] ; then
                exec_start='ExecStart=/usr/bin/env python3 -O /usr/local/bin/start_indy_node ${NODE_NAME} ${NODE_PORT} ${NODE_CLIENT_PORT}'
            else
                exec_start='ExecStart=/usr/bin/env python3 -O /usr/local/bin/start_indy_node ${NODE_NAME} ${NODE_IP} ${NODE_PORT} ${NODE_CLIENT_IP} ${NODE_CLIENT_PORT}'
            fi
            cat <<EOF > /etc/systemd/system/indy-node${i}.service
[Unit]
Description=Indy Node - Node${i}
Requires=indy-node-control.service

[Service]
EnvironmentFile=/etc/indy/indy${i}.env
$exec_start
User=indy
Group=indy
Restart=on-failure
RestartSec=10
StartLimitBurst=10
StartLimitInterval=200
TimeoutSec=300
LimitNOFILE=16384:65536

[Install]
WantedBy=multi-user.target
EOF
        done
        systemctl daemon-reload
        start_indy_node
        enable_indy_node
        sleep 2
    fi
    if [ -f "/var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis" ] ; then
        if [ -d '/vagrant/' ] ; then
            mkdir -p /vagrant/shared_configs/ledger
            cp /var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis /vagrant/shared_configs/ledger/genesis.txn
        fi
        if [ -d '/devlab/' ] ; then
            mkdir -p /devlab/shared_configs/ledger
            cp /var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis /devlab/shared_configs/ledger/genesis.txn
        fi
        mkdir -p /etc/ledger
        cp /var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis /etc/ledger/genesis.txn
    fi
}

function setup_wallet {
    echo "Setting up wallet"
    if [ "$cli_mode" == '1_4' ] ; then
        wallet_create_string="wallet create default pool_name=${LEDGER_NAME} key=${WALLET_KEY}"
    else
        wallet_create_string="wallet create default key=${WALLET_KEY}"
    fi
    if ! run_indy_cmd "wallet list" true | grep -q 'wallet%20list|default' ; then
        wallet_create_out=$(run_indy_cmd "$wallet_create_string")
        if [ $? -ne 0 ] ; then
            echo "Error trying to create wallet... Aborting" >&2
            exit 1
        fi
    else
        echo "Wallet 'default' already exists. Checking if we can open it..."
        check_out=$(run_indy_cmd "wallet open default key=${WALLET_KEY}")
        if [ $? -ne 0 ] ; then
            echo "$check_out"
            echo "Error trying to open wallet, maybe a bad key?... Aborting" >&2
            exit 1
        fi
    fi
    if ! run_indy_cmd "pool list" true | grep -q "pool%20list|${LEDGER_NAME}" ; then
        if ! find_genesis_file ; then
            return 1
        fi
        echo "Using genesis file: $GENESIS_FILE"
        pool_create_out=$(run_indy_cmd "pool create ${LEDGER_NAME} gen_txn_file=${GENESIS_FILE}")
        if [ $? -ne 0 ] ; then
            echo "$pool_create_out"
            echo "Failed creating pool for '${LEDGER_NAME}' in indy-cli"
            return 1
        fi
    else
        echo "Pool: ${LEDGER_NAME} already exists, skipping"
    fi
}

function start_indy_node {
    echo "Starting indy-node services"
    for i in {1..4} ; do
        echo "Starting Service: indy-node${i}"
        systemctl start indy-node${i}
    done
}

function stop_indy_node {
    echo "Stopping indy-node services"
    for i in {1..4} ; do
        echo "Stopping Service: indy-node${i}"
        systemctl stop indy-node${i}
    done
}

## - Main - ##
while [ $args_left -ge 1 ] ; do
    case "$1" in
        #Actions
        --help|-h)
            help
            exit 0
            ;;
        --install-indy|--install|-i)
            register_action install_indy
            add_req_var INTERNAL_IP
            add_req_var INDY_STARTING_PORT
            add_req_var LEDGER_NAME
            add_req_var WALLET_KEY
            ;;
        --install-indy-cli|--cli|-I)
            register_action install_indy_client
            ;;
        --mint)
            register_action mint_tokens
            add_req_var MINT_TOKENS_AMOUNT
            add_req_var PAYMENT_ADDRESS
            add_req_var SOVTOKEN
            add_req_var SOVTOKENFEES
            add_req_var WALLET_KEY
            ;;
        --provision|-p)
            provision=true
            ;;
        --seed-dids|--seed-ledger)
            register_action preseed_indy_wallet
            add_req_var LEDGER_NAME
            add_req_var WALLET_KEY
            ;;
        --set-fees|-f)
            register_action set_fees
            add_req_var SOVTOKEN
            add_req_var SOVTOKENFEES
            add_req_var FEES
            add_req_var WALLET_KEY
            ;;
        --set-taa|-t)
            register_action set_taa "$2"
            add_req_var LEDGER_NAME
            add_req_var WALLET_KEY
            if [ ! -z "$2" ] ; then
                shift
                let args_left=args_left-1
            fi
            ;;
        --start-indy-node|--start-indy|--start)
            register_action start_indy_node
            ;;
        --stop-indy-node|--stop-indy|--stop)
            register_action stop_indy_node
            ;;
        --unset-taa|-T)
            register_action set_taa ""
            add_req_var LEDGER_NAME
            add_req_var WALLET_KEY
            ;;
        #Options
        --env-file)
            if [ -f "$2" ] ; then
                source "$2"
                shift
                let args_left=args_left-1
            else
                echo "ERROR: Cannot find environment file: '$2'"
                exit 1
            fi
            ;;
        --genesis-file)
            if [ -f "$2" ] ; then
                GENESIS_FILE="$2"
                shift
                let args_left=args_left-1
            else
                echo "ERROR: Genesis file '$2' does not exists"
                exit 1
            fi
            ;;
        --indy-node-version|--indy-node)
            if [ ! -z "$2" ] ; then
                INDY_NODE="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        --indy-cli-version|--indy-cli)
            if [ ! -z "$2" ] ; then
                INDY_CLI="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        --libindy-version|--libindy)
            if [ ! -z "$2" ] ; then
                LIBINDY="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        --libsovtoken-version|--libsovtoken)
            if [ ! -z "$2" ] ; then
                LIBSOVTOKEN="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        --skip-fees|-F)
            FEES_ENABLE=false
            ;;
        --skip-mint|-M)
            MINT_TOKENS=false
            ;;
        --sovtoken-version|--sovtoken)
            if [ ! -z "$2" ] ; then
                SOVTOKEN="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        --sovtokenfees-version|--sovtokenfees)
            if [ ! -z "$2" ] ; then
                SOVTOKENFEES="$2"
                shift
                let args_left=args_left-1
            else
                echo "Missing parameter argument for '$2'"
                exit 1
            fi
            ;;
        *)
            echo "Unknown argument: $1"
    esac
    shift
    let args_left=args_left-1
done

##Set fees if specified
if [ "$provision" == true ] ; then
    provision_ledger
    if [ "$FEES_ENABLE" == true ] ; then
        register_action set_fees
        add_req_var SOVTOKEN
        add_req_var SOVTOKENFEES
        add_req_var FEES
        add_req_var WALLET_KEY
    fi
fi

if [ ${#to_do[@]} -eq 0 ] ; then
    echo "No action was specified:"
    help
    exit 1
fi

#Check for any missing required vars
if ! check_req_vars_set ${req_vars[@]} ; then
    echo -e "Missing some required variables!\nSet corresponding options and try again.\nexiting...\n\n"
    exit 2
fi

# Run our to_do list
for ((id=0; id<$doid; id++)) ; do
    action=${to_do[$id]}
    opts=( ${to_do_args[$id]} )
    for ((oid=0; oid<${#opts[*]}; oid++)) ; do
        op=${opts[$oid]}
        if [ "$op" = "''" ] ; then
            opts[$oid]=""
        else
            opts[$oid]=${op//%20/ }
        fi
    done
        if [ "$disp_play_by_play" == true ] ; then
            echo "Running action: $action \"${opts[@]}\""
        fi
    $action "${opts[@]}"
    rc=$?
    if [ "$exit_on_action_error" == true ] ; then
        if [ $rc -gt 0 ] ; then
            echo "ERROR: Received return code of $rc running action: $action and opts: ${opts[@]}"
            exit 1
        fi
    fi
done
