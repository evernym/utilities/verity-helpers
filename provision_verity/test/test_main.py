import pytest

from provision_verity.provision.main import get_args
from provision_verity.test.test_gen_token import test_args1 as gen_test_args
from provision_verity.test.test_provision_rest import test_args1 as rest_test_args
from provision_verity.test.test_provision_rest import mock_provision_agent
from provision_verity.provision import provision_rest
from provision_verity.provision.main import main


def test_get_args():
    test1 = {
        'SEED': 'HrWd38sqcr6FjQK1LNNCJxxkMbS2oR3W'
    }

    expected = {
        'SEED': (None, 'Should have SEED')
    }

    args1 = get_args(expected, test1)
    assert args1['SEED'] == 'HrWd38sqcr6FjQK1LNNCJxxkMbS2oR3W'

    with pytest.raises(Exception) as e_info:
        get_args(expected, {})
    assert e_info.value.args[0] == 'Should have SEED'

    expected = {
        'SEED': ('0000000000000000000000000000VAS1', 'Should have SEED')
    }
    args1 = get_args(expected, {})
    assert args1['SEED'] == '0000000000000000000000000000VAS1'


@pytest.mark.asyncio
async def test_main():
    # args = dict(gen_test_args)
    # args['TASK'] = "GEN_TOKENS"
    # v = await main(args)
    # assert len(v) == 3

    provision_rest.provision_agent = mock_provision_agent(
        'AvYaRrKAQZbt37Wrn9z1Wc',
        '6Qh6SKP3FWdnCX6FH2jBSxsacYN3WpasyEJmiX2R6rih'
    )
    args = dict(rest_test_args)
    args['TASK'] = "REST_PROVISION"
    v = await main(args)
    assert v['apiKey'] == \
           'PShxVnzYoGBM9WAFwqizahzdFDs1QGvLPnYgfgobnBb:' \
           '5PRUiQ3nHnUs8iWf4JqLQsR9LrnDBHnHFJi1eBLsxt4vSDK5FvByTRyEJ37yhaTuFQTSQtJ97stvH8x1HbdFETGJ'
