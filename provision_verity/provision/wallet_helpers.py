import string
import random

from vdrtools.did import create_and_store_my_did
from vdrtools.wallet import create_wallet, open_wallet


def random_string(length: int) -> str:
    return ''.join(random.choices(string.ascii_letters + string.digits, k=length))


def wallet_args():
    wallet_id = random_string(32)
    wallet_key = random_string(32)
    wallet_config = f'{{"id": "{wallet_id}"}}'
    wallet_cred = f'{{"key": "{wallet_key}"}}'
    return wallet_config, wallet_cred


async def setup_wallet(seed, conf, cred):
    await create_wallet(conf, cred)

    w_h = await open_wallet(conf, cred)

    did_config = f'{{"seed": "{seed}"}}'
    did_pair = await create_and_store_my_did(w_h, did_config)
    return w_h, did_pair
