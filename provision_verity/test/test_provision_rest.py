import pytest
from provision_verity.provision import provision_rest
import copy

test_args1 = {
    'SEED': 'HrWd38sqcr6FjQK1LNNCJxxkMbS2oR3W',
    'ENTERPRISE_ID': 'acme',
    'EVERNYM_ID': 'evernym',
    'AGENT_SEED': 'J9RCzVS4bNp8p5ro4W1JRAQc7zjS19LC',
    'VERITY_URL': 'http://vas-team1.pdev.evernym.com'
}


def mock_provision_agent(domain_did, verity_agent_verkey):
    async def mock(context, token):
        assert token is not None
        assert token['sig'] is not None
        new_context = copy.copy(context)
        new_context.domain_did = domain_did
        new_context.verity_agent_verkey = verity_agent_verkey
        return new_context

    return mock


@pytest.mark.asyncio
async def test_provision():
    provision_rest.provision_agent = mock_provision_agent(
        'AvYaRrKAQZbt37Wrn9z1Wc',
        '6Qh6SKP3FWdnCX6FH2jBSxsacYN3WpasyEJmiX2R6rih'
    )
    v = await provision_rest.provision_rest(test_args1)
    assert v['apiKey'] == \
           'PShxVnzYoGBM9WAFwqizahzdFDs1QGvLPnYgfgobnBb:' \
           '5PRUiQ3nHnUs8iWf4JqLQsR9LrnDBHnHFJi1eBLsxt4vSDK5FvByTRyEJ37yhaTuFQTSQtJ97stvH8x1HbdFETGJ'
