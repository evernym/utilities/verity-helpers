import base64
import datetime

from datetime import timezone
from vdrtools.crypto import crypto_sign
from vdrtools.wallet import close_wallet, delete_wallet

from provision_verity.provision.wallet_helpers import wallet_args, setup_wallet, random_string

task_args = {
    'SEED': (None, 'Key seed (env var SEED) must be defined'),
    'EVERNYM_ID': (None, 'Evernym Sponsor ID (env var EVERNYM_ID) must be defined'),
    'ENTERPRISE_ID': (None, 'Id for provisioning Enterprise (env var SEED) must be defined'),
    'TOKEN_COUNT': ('1', None)
}


async def gen_token(args, w_h, did_pair, timestamp=datetime.datetime.now(timezone.utc).isoformat(), nonce=None):
    if nonce is None:
        nonce = random_string(32)
    sponsee_id = args['ENTERPRISE_ID']
    sponsor_id = args['EVERNYM_ID']
    sponsor_verkey = did_pair[1]

    data = (nonce + timestamp + sponsee_id + sponsor_id).encode(encoding='utf-8')
    sig_data = await crypto_sign(w_h, sponsor_verkey, data)
    sig = str(base64.b64encode(sig_data), encoding='utf-8')

    return {
        'sponseeId': sponsee_id,
        'sponsorId': sponsor_id,
        'nonce': nonce,
        'timestamp': timestamp,
        'sig': sig,
        'sponsorVerKey': sponsor_verkey,
    }


async def gen_multiple_tokens(args):
    wallet_conf, wallet_cred = wallet_args()
    try:
        seed = args['SEED']
        wallet_handle, did_pair = await setup_wallet(seed, wallet_conf, wallet_cred)

        count = int(args['TOKEN_COUNT'])

        tokens = [await gen_token(args, wallet_handle, did_pair) for _ in range(count)]
        await close_wallet(wallet_handle)

        return tokens
    finally:
        await delete_wallet(wallet_conf, wallet_cred)
