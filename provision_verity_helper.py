#!/usr/bin/python

import argparse
import fcntl
import getpass
import json
import logging
import os
import random
import re
import ssl
import socket
import subprocess
import sys
import tempfile
import time


#This should be backwards and forwards compatible with python2 or 3
try:
    #For python3
    from urllib import request as url_request
    urlparse = url_request.urlparse
    from pathlib import Path
    text_input = input
except ImportError:
    #For python2
    import urllib2 as url_request
    urlparse = url_request.urlparse.urlparse
    text_input = raw_input
    try:
        from pathlib2 import Path
    except ImportError:
        class Path(object):
            @staticmethod
            def home(self=None):
                return os.path.expanduser('~')

ENVIRONMENTS = {
    'demo': {
        'sponsor_required': True,
        'sponsor_id': 'evernym-demo-sponsor',
        'vault_path': '',
        'vault_key': 'seed',
        'verity_url': ''
    },
   'manual': {
        'sponsor_required': True,
        'sponsor_id': None,
        'verity_url': None
    }
}
LOGGER = None
LOGGING_LEVELS = {
    'trace': 9,
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL,
    'notset': logging.NOTSET
}
PROVISION_VERITY_IMAGE = ''
ENV_OPTS = []
VAULT_ADDR = ''

##-Classes-##
class Command(object):
    """
    Run a command, and return either stdout as a string or an array of strings
    split by line

    Args:
        path: str, or list for the location of the process to run
        args: list
        ignore_nonzero_rc: bool, whether errors should create logs
        interactive: bool, whether the process should be "interactive"
        split: bool
        suppress_error_out: bool
        stdin: FileHandle, of stdin to send to the process
        timeout: integer, in minutes before the process is aborted <=0 mean no
            timeout. Default=0
        log_output: bool, whether to send the output of the command to the logger
        logger: Logger object to use for messages
    """
    def __init__(self, path, args=None, ignore_nonzero_rc=False, interactive=False, split=True, suppress_error_out=False, stdin=None, timeout=0, log_output=False, logger=None, **kwargs):
        """
        Initialize the command object
        """
        ignored_opts = kwargs
        if not args:
            self.args = []
        else:
            self.args = list(filter(lambda x: x != None, args))
        self.ignore_nonzero_rc = ignore_nonzero_rc
        self.interactive = interactive
        self.path = path
        self.real_path = None
        if logger:
            self.log = logger
        else:
            self.log = logging.getLogger('Command')
        self.log_output = log_output
        self.split = split
        self.suppress_error_out = suppress_error_out
        self.stdin = stdin
        self.timeout = timeout
        self.stdout = []
        self.stderr = []
        self.ctime = time.time()
        self.proc = None
        if log_output and interactive:
            raise DevlabCommandError("ERROR: Setting both 'interactive' and 'log_output' to True won't work")
    def _precheck(self):
        """
        Does some preliminary checks to ensure that executing the script will
        be good, before trying

        Return:
            tuple where:
                First element is an integer... -1 mean failed. 0 means success
                Second element is a message, or the path that was found
        """
        found_path = self.path
        if isinstance(self.path, (list, tuple, set)):
            in_path = False
            for found_path in self.path:
                if os.access(found_path, os.X_OK):
                    in_path = True
                    break
            if not in_path:
                if not self.suppress_error_out:
                    self.log.error("Can't find executable here: %s", self.path)
                    return (-1, "Error! Can't find executable here: {}".format(self.path))
        else:
            if not os.access(self.path, os.X_OK):
                if not self.suppress_error_out:
                    self.log.error("Can't find executable here: %s", self.path)
                return (-1, "Error! Can't find executable here: {}".format(self.path))
        return (0, found_path)
    def _sanitize_string(self, string_to_sanitize): #pylint: disable=no-self-use
        """
        Take a string that needs to be sanitized, and do the following things to it:
            1) Decode it to ascii, ignoring anything that isn't ascii
            2) If there are escape sequences, add an ending escape sequence to the
            3) If there is no terminal, then strip out any escape sequences
        Args:
            string_to_sanitize: The string to sanitize
        Returns
            str
        """
        #self.log.debug("Got original string: '{}'".format("%r" % string_to_sanitize))
        if not string_to_sanitize:
            return string_to_sanitize
        sanitized = string_to_sanitize.decode('ascii', 'ignore')
        sanitized = sanitized.strip()
        if ISATTY:
            if '\033[' in sanitized:
                #Append ending escape sequence to the string
                sanitized += '\033[0m'
        else:
            #Remove ending escape from string
            ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
            sanitized = ansi_escape.sub('', sanitized)
        #self.log.debug("Converted to: '{}'".format("%r" % sanitized))
        return sanitized
    def _process_output(self, max_lines=100, flush=False):
        line_count = 0
        cur_check = 0
        max_checks = 100
        if flush:
            for pipe in (self.proc.stdout, self.proc.stderr):
                try:
                    pipe.flush()
                except AttributeError:
                    pass
        while cur_check <= max_checks or flush:
            stdout_empty = False
            stderr_empty = False
            try:
                stdout_line = None
                stderr_line = None
                if self.proc.stdout is not None:
                    try:
                        stdout_line = self._sanitize_string(self.proc.stdout.readline())
                    except IOError:
                        stdout_line = None
                if self.proc.stderr is not None:
                    try:
                        stderr_line = self._sanitize_string(self.proc.stderr.readline())
                    except IOError:
                        stderr_line = None
                if stdout_line:
                    if self.log_output:
                        self.log.info(stdout_line)
                    self.stdout.append(stdout_line)
                    line_count += 1
                else:
                    stdout_empty = True
                if stderr_line:
                    if self.log_output:
                        self.log.warning(stderr_line)
                    self.stderr.append(stderr_line)
                    line_count += 1
                else:
                    stderr_empty = True
                if not flush:
                    #If both pipes are empty, then return/break out.
                    if stdout_empty and stderr_empty:
                        break
                    #Even if there is still data in the pipes, return back control if max_lines is reached
                    if line_count >= max_lines:
                        break
                else:
                    #No new lines from either pipe, and process has ended. Flush complete
                    if (stdout_empty and stderr_empty) and self.proc.poll() is not None:
                        #Get any non-newline separate content that is left
                        if self.proc.stdout:
                            stdout_dangle = self._sanitize_string(self.proc.stdout.read())
                            if stdout_dangle:
                                if self.log_output:
                                    self.log.info(stdout_dangle)
                                self.stdout.append(stdout_dangle)
                        if self.proc.stderr:
                            stderr_dangle = self._sanitize_string(self.proc.stderr.read())
                            if stderr_dangle:
                                if self.log_output:
                                    self.log.error(stderr_dangle)
                                self.stderr.append(stderr_dangle)
                        break
            except OSError:
                pass
            cur_check += 1
    def _wait_for_proc(self):
        """
        Wait for the running process to finish running and process any output
        that the process has generated while waiting. This is also responsible
        for watching the process for any timeouts
        """
        #Check every .1 seconds if the process is hung or not.
        #hung means it waited longer than self.timeout
        self.log.debug("Watching process (pid=%s) for completion or if hung", self.proc.pid)
        while self.proc.poll() is None:
            cur_time = time.time()
            #Write any error messages from the process using our logger
            self._process_output()
            #If our process has been running longer than self.timeout
            #then we should see if it is hung or something
            if self.timeout > 0:
                if cur_time - self.ctime > self.timeout * 60:
                    self.log.warning("Command: '%s'(pid=%s): appears to be hung, attempting to stop and/or kill it", ' '.join([self.real_path] + self.args), self.proc.pid)
                    self.proc.terminate()
                    wait_count = 0
                    while self.proc.poll() is None:
                        if wait_count >= 20:
                            self.log.warning("Command: '%s'(pid=%s): Didn't die, forcefully killing it", ' '.join([self.real_path] + self.args), self.proc.pid)
                            self.proc.kill()
                            self.proc.wait()
                            self._process_output(flush=True)
                            self.proc.communicate()
                            break
                        time.sleep(1)
                        wait_count += 1
            time.sleep(0.01)
        #Write any remaining log messages in the pipe
        self._process_output(flush=True)
        #This is needed so that the process can clean up its stdout/err pipes
        self.proc.communicate()
    def die(self, graceful=True):
        """
        Make any running process go away

        Args:
            graceful: bool, whether to try and gracefully kill process (INT),
                to send a SIGKILL signal
        """
        if self.proc:
            if graceful:
                self.proc.send_signal(signal.SIGINT)
                wait_count = 0
                while self.proc.poll() is None:
                    if wait_count >= 20:
                        self.proc.kill()
                        self.proc.wait()
                        break
                    time.sleep(1)
                    wait_count += 1
            else:
                self.proc.kill()
                self.proc.wait()
    def run(self):
        """
        Execute the command

        Returns:
            tuple where:
                First Element is the return code of the command
                Second Element is either a list of strings OR a str (if split==false)
        """
        precheck_res = self._precheck()
        if precheck_res[0] < 0:
            return precheck_res
        self.real_path = precheck_res[1]
        subprocess_args = {
            'shell': False
        }
        if not self.interactive:
            subprocess_args['stdout'] = subprocess.PIPE
            subprocess_args['stderr'] = subprocess.PIPE
        if self.stdin:
            subprocess_args['stdin'] = self.stdin
        self.log.debug("Running command: '%s'", ' '.join([self.real_path] + self.args))
        self.ctime = time.time()
        self.proc = subprocess.Popen([self.real_path] + self.args, **subprocess_args)
        for strm in (self.proc.stdout, self.proc.stderr):
            if strm is not None:
                fno = strm.fileno()
                fl_nb = fcntl.fcntl(fno, fcntl.F_GETFL)
                fcntl.fcntl(fno, fcntl.F_SETFL, fl_nb | os.O_NONBLOCK)
        self._wait_for_proc()
        if self.proc.returncode > 0:
            if not self.suppress_error_out:
                if not self.ignore_nonzero_rc:
                    self.log.error("Command did not exit with successful status code (%s): '%s %s'", self.proc.returncode, self.real_path, ' '.join(self.args))
                if self.stdout and not self.log_output:
                    for line in self.stdout:
                        self.log.error(line)
                if self.stderr and not self.log_output:
                    for line in self.stderr:
                        self.log.error(line)
            out = self.stderr
            if not self.stderr:
                if self.stdout:
                    out = self.stdout
                else:
                    out = ''
        else:
            if self.stdout:
                out = self.stdout
            else:
                out = ''
        if not self.split:
            out = '\n'.join(out)
        return (self.proc.returncode, out)

class DockerHelper(object):
    """
    This is a helper for running docker commands
    """
    def __init__(self, filter_label=None, labels=None, common_domain=None, skip_checks=False):
        """
        Initialize the DockerHelper Object

        Args:
            filter_label: String of a label to filter on when querying docker
            labels: List of labels to apply to all objects created by
                DockerHelper
            common_domain: When running containers, use this domain as part of
                the container name.
        """
        self.log = logging.getLogger('DockerHelper')
        self.docker_bin_paths = (
            '/usr/bin/docker',
            '/usr/local/bin/docker',
            '/usr/sbin/docker',
            '/bin/docker'
        )
        self.filter_label = filter_label
        self.common_domain = common_domain
        self.opt_domainname = False
        if not skip_checks:
            self._pre_check()
        self.labels = labels
    def _pre_check(self):
        """
        Checks to make sure the script is being run as the root user, or a
        user that is able to talk to docker

        Returns None, but if access check fails, then exit
        """
        dchk = Command(self.docker_bin_paths, ['ps'], logger=self.log).run()
        if dchk[0] != 0:
            if os.geteuid() != 0:
                self.log.error("Cannot talk to docker, maybe try again as the root user?")
            else:
                self.log.error("Cannot talk to docker, maybe it isn't running?")
            sys.exit(1)
        dchk = Command(self.docker_bin_paths, ['run', '--help'], logger=self.log, split=False).run()
        if '--domainname' in dchk[1]:
            self.opt_domainname = True
    def build_image(self, name, tag, context, docker_file, apply_filter_label=True, build_opts=None, logger=None, network=None, **kwargs):
        """
        Build a docker image.

        Args:
            name: str, The name of the image
            tag: str or list, Tag(s) to attach to the image
            context: str, Path to the build context to use
            docker_file: str, Path to the Dockerfile to build from
            apply_filter_label: bool, whether to add self.filter as a label to
                the created image
            build_opts: list/tuple, indicating additional options to pass to
                'docker build' (OPTIONAL)
            logger: Logger object to send logs to instead of self.log (OPTIONAL)
            network: str, docker network to attach (OPTIONAL)
            kwargs: dict, Additional arguments to pass to the Command Object
        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        opts = [
            'build',
            '--force-rm'
        ]
        cmd_logger = self.log
        if logger:
            cmd_logger = logger
        if network:
            opts.append("--network={}".format(network))
        if build_opts:
            opts += build_opts
        if self.labels:
            for label in self.labels:
                opts += [
                    '--label',
                    label
                ]
        if self.filter_label and apply_filter_label:
            opts.append('--label={}'.format(self.filter_label))
        if isinstance(tag, list):
            for btag in tag:
                opts += ['-t', '{}:{}'.format(name, btag)]
        else:
            opts += ['-t', '{}:{}'.format(name, tag)]
        opts += [
            '-f',
            '-',
            context
        ]
        if os.path.isfile(docker_file):
            with open(docker_file) as stdin:
                cmd_ret = Command(
                    self.docker_bin_paths,
                    opts,
                    stdin=stdin,
                    logger=cmd_logger,
                    **kwargs
                ).run()
                return cmd_ret
        else:
            self.log.error("Cannot find docker_file: %s", docker_file)
        return (1, ['Cannot find docker_file: {}'.format(docker_file)])
    def create_network(self, name, cidr=None, driver='bridge', device_name=None):
        """
        Create a docker network

        Args:
            name: str, Name of the network to create
            cidr: str, CIDR Notation for the network
        """
        opts = [
            'network',
            'create',
            '--subnet',
            cidr,
            '--driver',
            driver
        ]
        if self.labels:
            for label in self.labels:
                opts += [
                    '--label',
                    label
                ]
        if self.filter_label:
            opts.append('--label={}'.format(self.filter_label))
        if device_name:
            opts.append('--opt')
            opts.append('com.docker.network.bridge.name={}'.format(device_name))
        opts.append(name)
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            logger=self.log
        ).run()
        return cmd_ret
    def exec_cmd(self, name, cmd, background=False, interactive=True, ignore_nonzero_rc=False, logger=None, exec_opts=None, **kwargs):
        """
        Run a command inside of another container

        Args:
            name: str, The name of the container where the command should be run
            cmd: str, Command to run inside the container.
            background: Run the container in the background. (OPTIONAL)
            interactive: bool, whether or not the docker command could require
                console input etc... (OPTIONAL)
            ignore_nonzero_rc: bool indicating whether errors should create
                logs. (OPTIONAL)
            logger: Logger object to send logs to instead of self.log (OPTIONAL)
            exec_opts: list/tuple, indicating additional options to pass to
                'docker exec'. (OPTIONAL)
        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        ignored_opts = kwargs
        opts = [
            'exec',
        ]
        cmd_logger = self.log
        if exec_opts:
            opts += exec_opts
        if background:
            opts.append("--detach")
        if interactive:
            opts.append("-it")
        if logger:
            cmd_logger = logger
        opts += [
            name
        ]
        opts += shlex.split(cmd)
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            logger=cmd_logger,
            interactive=interactive,
            ignore_nonzero_rc=ignore_nonzero_rc,
            **kwargs
        ).run()
        return cmd_ret
    def get_containers(self, return_all=False):
        """
        List of containers that have been created

        Args:
            return_all: bool, whether or not to return all containers
                regardless of the filter set.

        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of dicts from docker if successful,
                    else a list of strings from the output of the command
        """
        opts = [
            'ps',
            '-a'
        ]
        if self.filter_label and not return_all:
            opts.append('--filter')
            opts.append('label={}'.format(self.filter_label))
        opts.append('--format')
        opts.append('{{.ID}},{{.Status}},{{.Names}}')
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            logger=self.log
        ).run()
        containers = []
        if cmd_ret[0] == 0:
            for cres in cmd_ret[1]:
                container_id, status, name = cres.split(',')
                containers.append({
                    'id': container_id,
                    'name': name,
                    'status': status
                })
            return (cmd_ret[0], containers)
        return cmd_ret
    def get_images(self, return_all=False):
        """
        List of images that docker has

        Args:
            return_all: bool, whether or not to return all images regardless of
                the filter set.

        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        opts = [
            'images'
        ]
        if self.filter_label and not return_all:
            opts.append('--filter')
            opts.append('label={}'.format(self.filter_label))
        opts.append('--format')
        opts.append('{{.Repository}}:{{.Tag}}')
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            logger=self.log
        ).run()
        return cmd_ret
    def get_networks(self, return_all=False):
        """
        List of networks that docker has

        Args:
            return_all: bool, whether or not to return all networks regardless
                of the filter set.

        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of dicts from docker if successful,
                    else a list of strings from the output of the command
        """
        opts = [
            'network',
            'list'
        ]
        if self.filter_label and not return_all:
            opts.append('--filter')
            opts.append('label={}'.format(self.filter_label))
        opts.append('--format')
        opts.append('{{.ID}},{{.Name}},{{.Driver}},{{.Scope}}')
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            logger=self.log
        ).run()
        networks = []
        if cmd_ret[0] == 0:
            for nres in cmd_ret[1]:
                network_id, name, driver, scope = nres.split(',')
                networks.append({
                    'id': network_id,
                    'name': name,
                    'driver': driver,
                    'scope': scope
                })
            return (cmd_ret[0], networks)
        return cmd_ret
    def inspect_container(self, container):
        """
        Grabs the inspection data (docker inspect) for a container

        Args:
            container: String of the container you want to inspect

        Return dict
        """
        ret = {}
        cmd_ret = Command(
            self.docker_bin_paths,
            [
                'inspect',
                container
            ],
            split=False,
            logger=self.log
        ).run()
        if cmd_ret[0] == 0:
            ret = json.loads(cmd_ret[1])
        return ret
    def prune_images(self, prune_all=False):
        """
        Prune images from docker

        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        opts = [
            'image',
            'prune'
        ]
        if self.filter_label and not prune_all:
            opts.append('--filter')
            opts.append('label={}'.format(self.filter_label))
        opts.append('-f')
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            logger=self.log
        ).run()
        return cmd_ret
    def pull_image(self, image, **kwargs):
        """
        Pull an image from docker repos

        Args:
            image: String of the image you want to pull. ie ubuntu:16.04 etc...
            kwargs: dict, Additional arguments to pass to the Command object

        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        opts = [
            'image',
            'pull',
        ]
        opts.append(image)
        if 'logger' not in kwargs:
            kwargs['logger'] = self.log
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            **kwargs
        ).run()
        return cmd_ret
    def rm_container(self, name, force=True):
        """
        Remove a container

        Args:
            name: str, Name of the image to remove
        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        opts = [
            'rm'
        ]
        if force:
            opts.append('-f')
        opts.append(name)
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            logger=self.log
        ).run()
        return cmd_ret
    def rm_image(self, name):
        """
        Remove an image

        Args:
            name: str, Name of the image to remove
        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        cmd_ret = Command(
            self.docker_bin_paths,
            [
                'rmi',
                '-f',
                name
            ],
            logger=self.log
        ).run()
        return cmd_ret
    def run_container(self, image, name, network=None, ports=None, background=True, interactive=False, ignore_nonzero_rc=False, cmd=None, logger=None, mounts=None, systemd_support=False, run_opts=None, **kwargs): #pylint: disable=too-many-arguments
        """
        Run a docker_container

        Args:
            image: str, The name of the image to use for the container
            name: str, The name of the container (this also sets the hostname)
            network: str, docker network to attach
            cmd: str, Command to run inside the container. (OPTIONAL)
            ports: list/tuple, of ports to publish to the host. (OPTIONAL)
            background: Run the container in the background. (OPTIONAL)
            interactive: bool, whether or not the docker command could require
                console input etc... (OPTIONAL)
            ignore_nonzero_rc: bool indicating whether errors should create
                logs. (OPTIONAL)
            logger: Logger object to send logs to instead of self.log (OPTIONAL)
            mounts: list/tuple, Of volume mounts to pass. (OPTIONAL)
            run_opts: list/tuple, indicating additional options to pass to
                'docker run'. (OPTIONAL)
            systemd_support: bool, whether to enable opts to let systemd work
                inside the container. (OPTIONAL)
        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        ignored_opts = kwargs
        opts = [
            'run',
        ]
        cmd_logger = self.log
        if run_opts:
            opts += run_opts
        if self.labels:
            for label in self.labels:
                opts += [
                    '--label',
                    label
                ]
        if self.filter_label:
            opts.append('--label={}'.format(self.filter_label))
        if background:
            opts.append("--detach")
        if network:
            opts.append("--network={}".format(network))
        if systemd_support:
            opts += [
                '--tmpfs=/run',
                '--tmpfs=/run/lock',
                '--tmpfs=/tmp',
                '--volume=/sys/fs/cgroup:/sys/fs/cgroup:ro',
                '-t' #This is needed so that 'docker logs' will show systemd output
            ]
        if mounts:
            for mount in mounts:
                opts.append('--volume={}'.format(mount))
        if ports:
            for port in ports:
                opts.append('--publish={}'.format(port))
        if interactive:
            opts.append('-it')
        if logger:
            cmd_logger = logger
        opts += [
            '--name',
            name
        ]
        if self.common_domain:
            if self.opt_domainname:
                opts += [
                    '--hostname',
                    name,
                    '--domainname',
                    self.common_domain
                ]
            else:
                opts += [
                    '--hostname',
                    '{}.{}'.format(name, self.common_domain)
                ]
        else:
            opts += [
                '--hostname',
                name
            ]
        opts.append(image)
        if cmd:
            opts += shlex.split(cmd)
        cmd_ret = Command(
            self.docker_bin_paths,
            opts,
            logger=cmd_logger,
            interactive=interactive,
            ignore_nonzero_rc=ignore_nonzero_rc,
            **kwargs
        ).run()
        return cmd_ret
    def start_container(self, name, ignore_nonzero_rc=False):
        """
        Start an already existing container

        Args:
            name: str, Name of the container to start
            ignore_nonzero_rc: bool, whether or not we should care if the rc not 0
        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        cmd_ret = Command(
            self.docker_bin_paths,
            [
                'start',
                name
            ],
            ignore_nonzero_rc=ignore_nonzero_rc,
            logger=self.log
        ).run()
        return cmd_ret
    def stop_container(self, name):
        """
        Stop an already existing container

        Args:
            name: str, Name of the container to stop
        Returns:
            tuple where:
                First Element is the return code from docker
                Second Element is a list of strings of the output from docker
        """
        cmd_ret = Command(
            self.docker_bin_paths,
            [
                'stop',
                name
            ],
            logger=self.log
        ).run()
        return cmd_ret

class VaultAuthError(Exception):
    pass

class Vault:
    """
    Insert your own Vault class here.
    """
    def __init__(
            self,
            vault_addr,
            insecure=False
    ):
        """
        Takes all the things that are needed to communicate and request data from a
        vault server

        Args:
            vault_addr: String
                The URL where vault can be found
            insecure: Bool
                Whether or not to disable ssl cert checks when talking to vault
                Default=False
        """
        assert vault_addr
        self.config = {
            'auth_token': None,
            'auth_path': None,
            'auth_user': None,
            'auth_pass': None,
            'vault_addr': vault_addr
        }
        self.insecure = insecure
        self.log = logging.getLogger('Vault-{}'.format(vault_addr))
    def _vault_request(self, path, payload=None, timeout=5, skip_auth_check=False):
        """
        Make a request to Vault

        Args:
            path: str
                vault path (URI) where the request is sent
            payload: dict
                Optional payload of dict values to send
            insecure: bool

        Returns Tuple
            Element 1: Bool: Indicating success
            Element 2: Str: Response from the request to the vault server
        """
        pass

   def check_vault_auth(self):
        """
        Looks up the token from vault, which should tells us whether the token
        works or not

        Returns Bool indicating success or failure
        """
        pass

   def check_vault_listening(self, max_wait=20):
        """
        Checks and waits for a maximum 'max_wait' seconds for the vault server
        to be listening on its port
        """
        pass

    def get(self, path):
        """
        Requst a secret from vault path secret store

        Args:
            path: str
                path to request from vault

        Returns Str of response from the request to the vault server
        """
        pass

   def login(
            self,
            auth_path='/v1/auth/ldap',
            auth_token=None,
            auth_user=None,
            auth_pass=None):
        """
        Logs into the vault server and stores a token

        Args:
            auth_token: String
                    Authenticate using a known vault token
            auth_path: String
                The auth path endpoint to use for user/pass authentication
            auth_user: String
                The username to use for authentication
            auth_pass: String
                The password to use for user authentication

        Returns True if login is successful, and raises VaultAuthError if there
        is a failure
        """
        pass

   def post(self, path, body, timeout=5):
        """
        Execute a POST command against vault

        Args:
            path: String
                path for POST comamnd to go to on the vault server
            data: Dict
                The dict to send as the POST body.
            timeout: int
                Timeout to wait for response from the server

        Returns Dict of response from the POST to the vault server
        """
        pass

    def renew_self_token(self):
        """
        Sends a request to renew the vault token

        Returns Bool indicating success for failure
        """
        pass

##-Functions-##
def action_gen_tokens(env_opts):
    env_opts.append('TASK=GEN_TOKENS')
    env_opts.append('TOKEN_COUNT={}'.format(ARGS.count))

def action_rest(env_opts):
    env_opts.append('TASK=REST_PROVISION')
    #Override verity_url if passed, else pull from hard coded environments, else fail
    if ARGS.verity_url:
        env_opts.append('VERITY_URL={}'.format(ARGS.verity_url))
    elif SELECTED_ENV['verity_url']:
        env_opts.append('VERITY_URL={}'.format(SELECTED_ENV['verity_url']))
    else:
        LOGGER.error("You MUST supply a verity url (see... --verity-url)")
        sys.exit(1)
    #Provide agent_see if passed
    if ARGS.agent_seed:
        env_opts.append('AGENT_SEED={}'.format(ARGS.agent_seed))

def action_vault_auth(env_opts=None):
    global VAULT
    VAULT = Vault(vault_addr=VAULT_ADDR)
    try:
        VAULT.login(
            auth_user=get_user_input('Enter your vault username'),
            auth_pass=get_user_input('Enter your vault password', secure=True)
        )
        return True
    except Exception:
        return False

def check_custom_registry():
    """
    Check to make sure that you have logged into gitlab.com has been logged into
    """
    docker_config = {
        'auths': {}
    }
    docker_config_loaded = False
    image_host = parse_docker_image_string(PROVISION_VERITY_IMAGE)['host']
    if image_host:
        if not docker_config_loaded:
            docker_config_path = '{}/.docker/config.json'.format(Path.home())
            if os.path.isfile(docker_config_path):
                with open(docker_config_path) as dcf:
                    docker_config.update(json.load(dcf))
            docker_config_loaded = True
        try:
            auth_str = docker_config['auths'][image_host] #pylint: disable=unused-variable
        except KeyError:
            #Need to auth to the docker registry
            LOGGER.error("This project is using a docker image hosted on the custom registry: {custom_repo}, and you appear to have never authenticated to it. \n\nPlease execute:\n    docker login {custom_repo}\n\nThen try again".format(custom_repo=image_host))
            sys.exit(1)

def get_user_input(prompt='Enter a value (default={default})', secure=False, allow_empty=False, default=None, yesno=False):
    prompt = '{}: '.format(prompt)
    while True:
        if secure:
            resp = getpass.getpass(prompt.format(default=default))
        else:
            resp = text_input(prompt.format(default=default))
        if not resp:
            if default:
                resp = default
        if yesno:
            if resp.lower() in ('yes', 'y'):
                return True
            elif resp.lower() in ('no', 'n'):
                return False
            else:
                print("Invalid response, must be one of 'yes, y, no, n'")
                continue
        if allow_empty:
            return resp
        else:
            if not resp:
                print("You must provide an answer")
                continue
            else:
                return resp

def is_valid_hostname(hostname):
    """
    Takes a hostname and tries to determine if it is valid or not

    Args:
        hostname: String of the hostname to check

    Returns:
        Boolean if it is valid or not
    """
    # Borrowed from https://stackoverflow.com/questions/2532053/validate-a-hostname-string
    if len(hostname) > 255:
        return False
    if hostname.endswith("."): # A single trailing dot is legal
        hostname = hostname[:-1] # strip exactly one dot from the right, if present
    disallowed = re.compile(r"[^A-Z\d-]", re.IGNORECASE)
    return all( # Split by labels and verify individually
        (label and len(label) <= 63 # length is within proper range
         and not label.startswith("-") and not label.endswith("-") # no bordering hyphens
         and not disallowed.search(label)) # contains only legal characters
        for label in hostname.split("."))

def logging_init(level):
    """
    Initialize and create initial LOGGER
    level is a String of one of:
        'trace'
        'debug'
        'info'
        'warning'
        'error'
        'critical'
        'notset'
    """
    #Initialize logging
    #print("Initializing Logging")
    global LOGGER
    try:
        LOG_LEVEL = int(level)
    except ValueError:
        LOG_LEVEL = LOGGING_LEVELS[level.lower()]
    logging.addLevelName(9, "TRACE")
    logging.Logger.trace = logger_trace
    logging.basicConfig(
        level=LOG_LEVEL,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    )
    LOGGER = logging.getLogger("Main")

def logger_trace(self, message, *args, **kws):
    """
    This is a dangling function to be added to logging.Logger and provides
    a new trace log level
    """
    if self.isEnabledFor(9):
        self._log(9, message, args, **kws)

def parse_docker_image_string(image):
    """
    Take a docker image string and break it into different parts:
        image: The original image passed in
        host: Custom registry host. None if it is the default docker hub
        bare_image: The image with the tag and host stripped off
        tag: The tag of the image
    """
    parsed = {
        'image': None,
        'bare_image': '',
        'tag': None,
        'host': None
    }
    parse_image_split = image.split('/')
    host_check = parse_image_split[0]
    host_check_no_port = host_check
    if ':' in host_check:
        host_port_split = host_check.split(':')
        host_port_check = re.match(r'[0-9]{2,5}', host_port_split[1])
        if host_port_check:
            host_check_no_port = host_port_split[0]
    if ':' in host_check and is_valid_hostname(host_check_no_port):
        parsed['host'] = host_check
        parsed['bare_image'] = '/'.join(parse_image_split[1:])
    else:
        parsed['bare_image'] = image
    parsed['image'] = image
    if ':' in parsed['bare_image']:
        tag_split = parsed['bare_image'].split(':')
        parsed['bare_image'], parsed['tag'] = tag_split
    else:
        parsed['tag'] = 'latest'
    return parsed

def port_check(host, port, timeout=2):
    """
    Perform a basic socket connect to 'host' on 'port'.

    Args:
        host: String of the host/ip to connect to
        port: integer of the port to connect to on 'host'
        timeout: integer indicating timeout for connecting. Default=2

    Returns:
        Boolean whether the connection was successful or now
    """
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    skt.settimeout(timeout)
    try:
        skt.connect((host, int(port)))
        skt.shutdown(socket.SHUT_RDWR)
        return True
    except Exception: ##pylint: disable=broad-except
        return False
    finally:
        skt.close()

def rand_str(length):
    res = ''
    lower = 'abcdefghijklmnopqrstuvwxyz'
    upper = lower.upper()
    num = '01234567890'
    charset = lower + upper + num
    for i in range(0, length): #pylint: disable=unused-variable
        res += random.choice(charset)
    return res

## -- Main -- ##
#Check to see if we are attached to a TTY
try:
    ISATTY = sys.stdout.isatty()
except AttributeError:
    ISATTY = False
if __name__ == '__main__':
    #Define arguments
    ARG_PARSER = argparse.ArgumentParser(description='Main arguments')
    ARG_PARSER.add_argument('-l', '--log-level', type=str, choices=list(LOGGING_LEVELS.keys()), default='info', help="Set the log level output")
    ARG_PARSER.add_argument('-e', '--environment', type=str, choices=list(ENVIRONMENTS.keys()), help="Set the environment for provisioning tokens")
    ARG_PARSER.add_argument('-i', '--enterprise-id', type=str, help="The ID for the enterprise/entity requesting the token(s)")
    ARG_PARSER.add_argument('-s', '--seed', type=str, default='', help="Override the built-in value for the seed. Mostly useful when using the 'manual' environment")
    ARG_PARSER.add_argument('-t', '--service-type', type=str, choices=('cas', 'vas'), default='vas', help="Set the service for the environment")
    ARG_PARSER.add_argument('-I', '--sponsor-id', type=str, default='', help="Override the built-in value for the sponsor id (EVERNYM_ID). Mostly useful when using the 'manual' environment")
    ARG_PARSER.add_argument('-R', '--sponsor-notrequired', action='store_true', help="Override whatever build-in value for sponsor being required and disable the requiring of a sponsor seed. Mostly useful when using the 'manual' environment pointing at an environment doesn't require sponsors for provisioning")
    ARG_PARSER.add_argument('-m', '--multiline', action='store_true', default=False, help='When emiting tokens, output them as multiline objects instead of single line')
    ARG_PARSER.add_argument('-n', '--docker-network', type=str, default='', help='When running, attach to this docker network. (Primarily used for local dev envs)')
    SUBPARSERS = ARG_PARSER.add_subparsers(help="Modes")

    GEN_TOKENS_PARSER = SUBPARSERS.add_parser('gen_tokens', help="Mode for generating provision tokens for sponsee")
    GEN_TOKENS_PARSER.add_argument('-c', '--count', type=int, default=1, help="Number of tokens to generate. ")
    GEN_TOKENS_PARSER.set_defaults(func=action_gen_tokens)

    REST_PROV_PARSER = SUBPARSERS.add_parser('rest', help="Mode for generating a token and provisioning a REST api token")
    REST_PROV_PARSER.add_argument('-c', '--count', type=int, default=1, help="Number of tokens to generate. ")
    REST_PROV_PARSER.add_argument('-s', '--agent-seed', type=str, help="OPTIONAL agent seed to use when provisioning the REST token. If blank a random one will be chosen")
    REST_PROV_PARSER.add_argument('-u', '--verity-url', type=str, help="Override the built-in value for the verity-url. Mostly userful when using the 'manual' environment")
    REST_PROV_PARSER.set_defaults(func=action_rest)

    VAULT_AUTH_PARSER = SUBPARSERS.add_parser('vault-auth', help='Just authenticate to vault and get a new token. Then exit')
    VAULT_AUTH_PARSER.set_defaults(func=action_vault_auth)

    #Parse our args
    ARGS = ARG_PARSER.parse_args()
    logging_init(level=ARGS.log_level)

    if ARGS.func == action_vault_auth:
        sys.exit(ARGS.func())

    #Create our DockerHelper object
    DOCKER = DockerHelper()

    #Override sponsor_id
    if ARGS.sponsor_id:
        SELECTED_ENV['sponsor_id'] = ARGS.sponsor_id
    #We were told to disable sponsor seed requirement for provisioning
    if ARGS.sponsor_notrequired:
        LOGGER.warning("Disabling sponsor required seed for provisioning per --sponsor-notrequired argument")
        SELECTED_ENV['sponsor_required'] = False
        if not SELECTED_ENV['sponsor_id']:
            LOGGER.warning("No sponsor id was set (--sponsor-id), but since sponsor is not required, setting to 'NotSet'")
            SELECTED_ENV['sponsor_id'] = 'NotSet'
    #Checks for required vars
    if not ARGS.environment:
        LOGGER.error("You must supply an environment with: -e/--environment")
        sys.exit(1)
    if not ARGS.enterprise_id:
        LOGGER.error("You must supply an environment with: -i/--enterprise-id")
        sys.exit(1)
    SELECTED_ENV = ENVIRONMENTS[ARGS.environment]
    if not SELECTED_ENV['sponsor_id']:
        LOGGER.error("You must supply a sponsor id (aka EVERNYM_ID). See --sponsor-id")
        sys.exit(1)
    #Get SEED from vault if not passed as arg
    if ARGS.seed:
        SEED = ARGS.seed
    else:
        if SELECTED_ENV['sponsor_required']:
            #Retrieve secrets from vault
            VAULT = Vault(vault_addr=VAULT_ADDR)
            try:
                LOGGER.info("Attempting to log into vault")
                VAULT.login()
            except VaultAuthError:
                print("You need to (re)auth to the vault server at: {}".format(VAULT_ADDR))
                action_vault_auth()
            LOGGER.info("Looking up seed for environment: {}".format(ARGS.environment))
            SELECTED_ENV['vault_path'] = SELECTED_ENV['vault_path'].format(service_type=ARGS.service_type, **SELECTED_ENV)
            SEED = VAULT.get(SELECTED_ENV['vault_path'])
            if not SEED:
                LOGGER.error("Failed getting seed from vault_path: %s", SELECTED_ENV['vault_path'])
                sys.exit(1)
            SEED = SEED.get(SELECTED_ENV['vault_key'], None)
        else:
            LOGGER.info("Generating a random seed as sponsor requirement is disabled")
            SEED = rand_str(32)
    if not SEED:
        LOGGER.error("You must supply a seed. See --seed")
        sys.exit(1)
    ENV_OPTS += [
        'SEED={}'.format(SEED),
        'EVERNYM_ID={}'.format(SELECTED_ENV['sponsor_id']),
        'ENTERPRISE_ID={}'.format(ARGS.enterprise_id),
    ]
    if ARGS.multiline:
        ENV_OPTS.append('MULTILINE_OUTPUT=true')
    ARGS.func(ENV_OPTS)
    check_custom_registry()
    LOGGER.info("Pulling latest docker image")
    DOCKER.pull_image(PROVISION_VERITY_IMAGE)
    LOGGER.info("Generating needed environment variables")
    with tempfile.NamedTemporaryFile() as ENV_FILE:
        RUN_OPTS = [
            '--env-file={}'.format(ENV_FILE.name),
            '--rm'
        ]
        LOGGER.debug("Contents of environment file '{}':".format(ENV_FILE.name))
        for EVAR in ENV_OPTS:
            if EVAR.startswith('SEED='):
                LOGGER.debug("  SEED=<REDACTED>")
            else:
                LOGGER.debug('  {}'.format(EVAR))
            ENV_FILE.file.write('{}\n'.format(EVAR).encode())
        ENV_FILE.file.flush()
        LOGGER.info("Executing provision_verity bits")
        run_container_args = {
            'image': PROVISION_VERITY_IMAGE,
            'name': 'provision_rest',
            'network': ARGS.docker_network,
            'background': False,
            'interactive': True,
            'mounts': None,
            'run_opts': RUN_OPTS
        }
        if ARGS.func == action_rest:
            if ARGS.count > 1:
                for i in range(ARGS.count):
                    print('Token #{}'.format(i+1))
                    PROC = DOCKER.run_container(**run_container_args)
                    print('')
            else:
                print('Token #1')
                PROC = DOCKER.run_container(**run_container_args)
        else:
            PROC = DOCKER.run_container(**run_container_args)
