import pytest

from provision_verity.provision.gen_token import *

test_args1 = {
    'SEED': 'HrWd38sqcr6FjQK1LNNCJxxkMbS2oR3W',
    'ENTERPRISE_ID': 'acme',
    'EVERNYM_ID': 'evernym',
    'TOKEN_COUNT': '3'
}


@pytest.mark.asyncio
async def test_wallet_setup():
    wallet_conf, wallet_cred = wallet_args()
    w_h, did = await setup_wallet(test_args1['SEED'], wallet_conf, wallet_cred)
    assert w_h is not None
    assert w_h is not 0
    assert did[0] == 'HGxwRX1NZ2rMjdLq7CVEjk'
    assert did[1] == '9sVaV45JGbEpf1L89AmMhBBUAFZvSL8h3TV3UUbmQuv1'


@pytest.mark.asyncio
async def test_gen_token():
    args = {
        'SEED': '000000000000000000000000Trustee1',
        'ENTERPRISE_ID': 'myId',
        'EVERNYM_ID': 'evernym-test-sponsorabc123',
        'TOKEN_COUNT': 1
    }

    wallet_conf, wallet_cred = wallet_args()

    w_h, did_pair = await setup_wallet(args['SEED'], wallet_conf, wallet_cred)
    token = await gen_token(args, w_h, did_pair, timestamp='2020-06-05T21:33:36.085Z', nonce='123')

    assert token['sig'] == 'ZkejifRr3txh7NrKokC5l2D2YcABUlGlAoFHZD9RapHHBfVtNnHgYux1RCAiEh4Q31VJE3C92T1ZnqDm1WlEAA=='
    assert token['sponseeId'] == 'myId'
    assert token['sponsorId'] == 'evernym-test-sponsorabc123'
    assert token['nonce'] == '123'
    assert token['timestamp'] == '2020-06-05T21:33:36.085Z'
    assert token['sponsorVerKey'] == 'GJ1SzoWzavQYfNL9XkaJdrQejfztN4XqdsiV4ct3LXKL'


@pytest.mark.asyncio
async def test_gen_multiple_tokens():
    tokens = await gen_multiple_tokens(test_args1)

    assert tokens[0]['nonce'] is not tokens[1]['nonce']
    assert tokens[0]['nonce'] is not tokens[2]['nonce']
    assert tokens[1]['nonce'] is not tokens[2]['nonce']
    pass
