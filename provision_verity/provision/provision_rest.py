import json

from base58 import b58encode
from vdrtools import crypto
from verity_sdk.protocols.v0_7.Provision import Provision
from verity_sdk.utils.Context import Context

from provision_verity.provision.gen_token import gen_multiple_tokens
from provision_verity.provision.gen_token import task_args as gen_token_args
from provision_verity.provision.wallet_helpers import random_string

task_args = dict(gen_token_args)
task_args.update(
    {
        'AGENT_SEED': (random_string(32), None),
        'VERITY_URL': (None, 'Verity Url (env var VERITY_URL) must be defined')
    }
)


async def gen_signature(wallet_handle: int, verkey: str):
    return b58encode(await crypto.crypto_sign(wallet_handle, verkey, verkey.encode('utf-8'))).decode('utf-8')


async def provision_agent(context, token):
    return await Provision(json.dumps(token)).provision(context)


async def provision_rest(args) -> dict:
    # verity_url, example: http://vas-team1.pdev.evernym.com
    verity_url = args['VERITY_URL']
    seed = args['AGENT_SEED']

    # create initial Context
    wallet_name = random_string(32)
    wallet_key = random_string(32)
    context = await Context.create(wallet_name, wallet_key, verity_url, seed=seed)

    verkey = context.sdk_verkey

    args['TOKEN_COUNT'] = 1
    token = await gen_multiple_tokens(args)
    token = token[0]
    context = await provision_agent(context, token)

    signature = await gen_signature(context.wallet_handle, verkey)
    api_key = '{}:{}'.format(verkey, signature)

    rtn = json.loads(context.to_json())
    del rtn['walletName']
    del rtn['walletKey']
    del rtn['walletPath']
    del rtn['endpointUrl']
    rtn['apiKey'] = api_key
    return rtn
